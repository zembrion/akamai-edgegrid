<?php
namespace Zembrion\API;

/**
 * Contains the headers and body of the response to an API request.
 */
class Response {
    public $body;
    public $headers;
    public $status_code;
    public $main_language;
    
    private $api;

    public function __construct($api, $response){
        $this->status_code = $response->status_code;
        $this->body = json_decode($response->body);
        $this->headers = $response->headers;                
        $this->api = $api;
    }    
}
