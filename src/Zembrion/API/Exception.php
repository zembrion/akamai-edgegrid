<?php
namespace Zembrion\API;

class Exception extends \Exception {
    public $response;
    
    public function __construct($response){
        $body = $response->body;
        if (isset($body->detail)){
            $message = isset($body->detail) ? $body->detail : $body->title;
        }else{
            $message = "Uknown Error";
        }
        
        parent::__construct('Returned with status code ' . $response->status_code . ': ' . $message);
        $this->response = $response;
    }
}
