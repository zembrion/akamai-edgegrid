<?php
namespace Zembrion\API;

class NotFoundException extends Exception {
	
	public function __construct($message, $code = 0, Exception $previous = null) {
        // algo de código    
        // asegúrese de que todo está asignado apropiadamente
        parent::__construct($message, $code, $previous);
    }

    // representación de cadena personalizada del objeto
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }	
}