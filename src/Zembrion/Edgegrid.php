<?php 
namespace Zembrion;

require_once 'Crypt/HMAC2.php';
use Crypt_HMAC2;


class Edgegrid {
	public $requests;

	public $clientToken;
	public $clientSecret;
	public $accessToken;
    public $host;
	public $hasher;
	public $url;

    public function __construct($base,$token,$secret,$access){
        $this->requests = new Requests;                
        $this->url = "https://".$base;
        $this->clientToken = $token;
        $this->clientSecret = $secret;
        $this->host = $base;
        $this->accessToken = $access;
        $this->hasher = new Crypt_HMAC2($this->clientSecret, "sha256");     
    }	

    public function get($path, $params = null){
        $url_params = '';
        if (is_array($params)){
            $url_params = '?' . http_build_query($params);
        }
        return $this->_call('GET', $path . $url_params);
    }  


    public function post($path, $data){
        $d = $data;
        return $this->_call('POST', $path,$d);
    }          

	public function gen_uuid() {
	    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',	        
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
	        mt_rand( 0, 0xffff ),
	        mt_rand( 0, 0x0fff ) | 0x4000,
	        mt_rand( 0, 0x3fff ) | 0x8000,
	        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
	    );
	}

    protected function _call($method, $path, $data = null){    	
    	$timestamp     =   gmstrftime("%Y%m%dT%H:%M:%S+0000");    	

    	$urlP = parse_url($this->url);    	

    	$request_data[] = $method;
    	$request_data[] = $urlP['scheme'];
    	$request_data[] = $this->host;
    	$request_data[] = $path;

        if ($data==null){
            $request_data[] = "\t";    
        }else{                                    
            $request_data[] = "\t".$this->hex2b64(hash("sha256",$data));
        }
        
    	
    	
    	$authData[] = "client_token=".$this->clientToken;
    	$authData[] = "access_token=".$this->accessToken;
    	$authData[] = "timestamp=".$timestamp;
    	$authData[] = "nonce=".$this->gen_uuid();
    	$auth_data =  implode(";", $authData);
    	$auth_data = 'EG1-HMAC-SHA256 '.$auth_data.";";

    	$request_data[] = $auth_data;

    	$string_to_sign = implode("\t",$request_data);

		$this->hasher->setKey($this->clientSecret);
		$sig = $this->s3_sign($timestamp);
		$this->hasher->setKey($sig);
		$signature = $this->s3_sign(utf8_encode($string_to_sign));

        $headers = [                             	
            'Authorization'=> $auth_data."signature=".$signature,
            'content-type' => 'application/xml',            
        ];

        if ($method=="POST"){            
            $headers['content-length'] = strlen($data);
        }
        
        $options = [
            'timeout' => 10,
            'useragent' => "Zembrion",
        ];
        
        
        $response = $this->requests->request($this->url . $path, $headers, $data, $method, $options);        

        $responseCode = new API\Response($this, $response);
        if ($responseCode->status_code == 404){
            throw new API\NotFoundException($response->body->detail);
        } elseif (!in_array($responseCode->status_code, [200, 201])){
            throw new API\Exception($responseCode);
        }
        
        
        return json_decode(json_encode(simplexml_load_string($response->body)),true);
    }

	public function s3_sign($StringToSign){	    
	    return $this->hex2b64($this->hasher->hash($StringToSign));
	}



	public function hex2b64($str){
		$raw = $str;
	    $raw = '';
	    for ($i = 0; $i < strlen($str); $i += 2) {
	        $raw .= chr(hexdec(substr($str, $i, 2)));
	    }
	    return base64_encode($raw);
	}    

}


