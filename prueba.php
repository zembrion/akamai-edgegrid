<?php
	require __DIR__ . '/vendor/autoload.php';

	$api = new Zembrion\Edgegrid(
		"base",
		'token',
		'secret',
		'access');

	try {		
		//GET LIVE EVENTS
		$response = $api->get('/config-media-live/v1/live/');			
		//CREATE NEW STREAM
		$apiCall = '/config-media-live/v1/live/eventshd-lh.akamaihd.net/stream';				
		$stream = getNewStream();
		$response = $api->post($apiCall,$stream);			
		print_r($response);		
	} catch (Exception $e) {
		die($e->getMessage());
	}
	
	
function getNewStream(){
	return '<?xml version="1.0" encoding="UTF-8"?><stream><stream-type>Universal Streaming Live</stream-type><stream-name>STREAMING_PHP</stream-name><primary-contact-name>Ariel Gonzalez</primary-contact-name><secondary-contact-name>Ariel Gonzalez</secondary-contact-name> <notification-email>ionvms@ionmicrosystems.com</notification-email> <encoder-settings> <primary-encoder-ip>190.104.217.74</primary-encoder-ip><backup-encoder-ip>190.104.217.74</backup-encoder-ip><password>mipassword1</password></encoder-settings><dvr-settings><dvr>disabled</dvr></dvr-settings></stream>';
}
